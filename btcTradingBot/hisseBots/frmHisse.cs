﻿using Newtonsoft.Json;
using pendata_hisse;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace hisseBots
{
    public partial class frmHisse : Form
    {
        #region Değişkenler

        public int sn { get; set; }
        public string siteConfig { get; set; }

        public string lastOperation { get; set; }

        public double operationAmount { get; set; }

        public double alinanMiktar { get; set; }
        public double SatilanMiktar { get; set; }
        public double toplamKar { get; set; }
        public double toplamZarar { get; set; }

        public DateTime operationDate { get; set; }

        public DateTime waitOperation { get; set; }

        public DateTime zamanStart { get; set; }

        public DateTime zamanEnd { get; set; }

        public bool decisionStepAlis1 { get; set; }

        public bool decisionStepAlis2 { get; set; }

        public bool decisionStepSatis1 { get; set; }

        public bool decisionStepSatis2 { get; set; }

        public bool isBogaSeason { get; set; }

        public bool isKesisti { get; set; }

        public bool isZarar { get; set; }


        string json;

        public enum logTip
        {
            error, warning, success, info
        }

        public int rowCount { get; set; }

        public int alCount { get; set; }

        public int seasonCount { get; set; }

        HttpWebRequest requestDagur { get; set; }

        CookieContainer cookieDagur { get; set; }

        public string hisseName { get; set; }

        public string hisseShortName { get; set; }
        //private static readonly TelegramBotClient Bot = new TelegramBotClient("563571585:AAHCTruK6lOkwHrs4slnOjA7U4TR3JB2l-0");

        #endregion

        #region Fonks

        public void setLog(string txt, Color renk)
        {
            try
            {
                if (renk == null)
                    renk = Color.Gray;

                this.rowCount++;
                if (this.rowCount > 500)
                {
                    helper.setDebug(this.txtLog.Text, "islemLoglari_paribuBot_");
                    this.rowCount = 0;
                    this.txtLog.Text = "";
                    this.txtLog.Refresh();
                    this.Refresh();
                    Application.DoEvents();
                }

                this.txtLog.SelectionStart = this.txtLog.TextLength;
                this.txtLog.SelectionLength = 0;

                this.txtLog.SelectionColor = renk;
                this.txtLog.AppendText(string.Format("[{0:HH:mm:ss}] {1}\r\n", DateTime.Now, txt));
                this.txtLog.SelectionColor = this.txtLog.ForeColor;

                this.txtLog.SelectionStart = this.txtLog.Text.Length;
                this.txtLog.ScrollToCaret();

                this.txtLog.Refresh();
                this.Refresh();
                Application.DoEvents();
            }
            catch
            {

            }
        }

        public void setLog(string txt, logTip tip = logTip.info)
        {
            Color renk;
            if (tip == logTip.error)
                renk = Color.Red;
            else if (tip == logTip.success)
                renk = Color.DarkGreen;
            else if (tip == logTip.warning)
                renk = Color.Orange;
            else
                renk = Color.LightGray;


            this.setLog(txt, renk);
        }

        public void setLogError(string txt)
        {
            this.setLog(txt, logTip.error);
        }

        public void setLogSuccess(string txt)
        {
            this.setLog(txt, logTip.success);
        }

        public void setLogWarning(string txt)
        {
            this.setLog(txt, logTip.warning);
        }

        /// <summary>
        /// request lere genel ayar yapmak için
        /// </summary>
        /// <param name="req"></param>
        /// <param name="cook"></param>
        private void setRequest(HttpWebRequest req, CookieContainer cook)
        {
            req.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3402.0 Safari/537.36";
            req.CookieContainer = cook;
        }

        private string getData(HttpWebRequest req)
        {
            string ret = "";

            req.Method = "GET";

            var response = (HttpWebResponse)req.GetResponse();
            ret = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return ret;
        }

        private string postData(HttpWebRequest req, string data)
        {
            string ret = "";

            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = data.Length;

            var dt = Encoding.ASCII.GetBytes(data);

            using (var stream = req.GetRequestStream())
            {
                stream.Write(dt, 0, dt.Length);
            }

            var response = (HttpWebResponse)req.GetResponse();

            ret = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return ret;
        }

        #endregion

        #region init & event
        public frmHisse()
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            this.sn = 0;
            rowCount = 0;
            cookieDagur = new CookieContainer();
            InitializeComponent();
        }

        private void frmHisse_Load(object sender, EventArgs e)
        {
            this.timer1.Enabled = true;
        }

        private void frmHisse_Shown(object sender, EventArgs e)
        {
            this.Text = "paribu Test Bot - ";

            this.Refresh();
            this.timer1.Tick += Timer1_Tick;
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            this.Refresh();
            this.sn++;

            if (this.sn % 5 == 0)
            {

                this.Refresh();
                this.timer1.Enabled = false;
                this.button1.Enabled = false;
                //this.start();
                start();
            }
        }

        #endregion
        private XmlNodeList getNodeList(string xmlText, string xPath)
        {

            var doc = new XmlDocument();
            doc.LoadXml(xmlText);
            return doc.SelectNodes(xPath);
        }

        private string getAttributeValue(XmlNode node, string attributeName)
        {
            try
            {
                var att = node.Attributes.GetNamedItem(attributeName);
                if (att == null)
                    return "";
                else
                    return (att.Value);
            }
            catch (Exception a)
            {
                MessageBox.Show(a.ToString());
                return "";
            }

        }

        private void start()
        {
            timer1.Enabled = false;

            XmlDocument doc = new XmlDocument();

            doc.Load(AppDomain.CurrentDomain.BaseDirectory + "config.xml");

            var uw = helper.getUW(Program.connectionMain);

            //hisse xx = new hisse(uw);
            //xx.name = "Garanti";
            //xx.name = "Garan";
            //xx.Save();
            //uw.CommitChanges();
            //
            //hisseAmounts aa = new hisseAmounts(uw);
            //aa.addDate = DateTime.Now;
            //aa.Save();
            //uw.CommitChanges();


            decisionStepAlis1 = false;
            decisionStepAlis2 = false;
            decisionStepSatis1 = false;
            decisionStepSatis2 = false;
            lastOperation = "";
            operationAmount = 0;
            alCount = 0;
            isBogaSeason = true;
            isKesisti = false;
            isZarar = false;
            waitOperation = DateTime.Now.AddYears(-3);
            toplamKar = 0;
            toplamZarar = 0;

            zamanEnd = Convert.ToDateTime(DateTime.Now.ToString("dd.MM.yyyy HH:00:00"));
            zamanStart = Convert.ToDateTime(DateTime.Now.AddMonths(-1).ToString("dd.MM.yyyy HH:00:00"));

            var startEpoch = (zamanStart - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            var endEpoch = (zamanEnd - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;

            string sqlHisseler = string.Format(@"SELECT * FROM hisse ");

            var sonucHisseler = helper.getRowInSql(uw, sqlHisseler);

            foreach (var itemHisse in sonucHisseler)
            {
                hisseName = itemHisse[1].ToString();
                hisseShortName = itemHisse[2].ToString();


                if (hisseName == "Garanti" && hisseShortName == "garan")
                {
                    try
                    {
                        string html = "";

                        //Uri url = new Uri("https://tvc4.forexpros.com/382fb5c6aced59f9a67dbe3e9c00857f/1564166817/10/10/63/history?symbol=19412&resolution=60&from=1546041600&to=1559088000");
                        Uri url = new Uri("https://tvc4.forexpros.com/382fb5c6aced59f9a67dbe3e9c00857f/1564166817/10/10/63/history?symbol=19412&resolution=60&from=1559088000&to=1566333300");
                        // Uri url = new Uri("https://tvc4.forexpros.com/382fb5c6aced59f9a67dbe3e9c00857f/1564166817/10/10/63/history?symbol=19412&resolution=60&from=" + startEpoch + "&to=" + endEpoch);

                        using (var client = new WebClient())
                        {
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            html = client.DownloadString(url);

                        }

                        var json = JsonConvert.DeserializeObject<hisseBots.RootObject>(html);

                        List<DateTime> dateList = new List<DateTime>();
                        List<double> btcCloseAmount = new List<double>();

                        foreach (var item in json.t)
                        {
                            var epoch = new DateTime(1970, 1, 1, 3, 0, 0, DateTimeKind.Utc);
                            dateList.Add(epoch.AddSeconds(item));
                        }

                        foreach (var item in json.c)
                        {
                            btcCloseAmount.Add(item);
                        }

                        ////////for (int i = 0; i < btcCloseAmount.Count; i++)
                        ////////{
                        ////////    string sql = string.Format(@"SELECT *
                        ////////                   FROM hisseAmounts 
                        ////////                   WHERE  dateHisse='{0}' and hisse=1", dateList[i].ToString("yyyy-MM-dd HH:mm:ss"));
                        ////////
                        ////////    var sonuc = helper.getRowInSql(uw, sql);
                        ////////
                        ////////    if (sonuc.Count == 0)
                        ////////    {
                        ////////        // ema7Amount,
                        ////////        //ema24Amount,
                        ////////        //rsiAmount,
                        ////////        //decision,
                        ////////
                        ////////        var sqll = string.Format(@"
                        ////////
                        ////////              	     INSERT   hisseBorsa.dbo.hisseAmounts
                        ////////              	             ( hisseAmount,
                        ////////                               dateHisse,
                        ////////                               addDate,
                        ////////                               hisse
                        ////////
                        ////////              	             )
                        ////////              	     VALUES
                        ////////              	             ( 
                        ////////              	                {0},
                        ////////                               '{1}',
                        ////////                               '{2}',
                        ////////                                1  
                        ////////              	             )
                        ////////                  ", btcCloseAmount[i].ToString().Replace(",", "."), dateList[i].ToString("yyyy-MM-dd HH:mm:ss"), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        ////////
                        ////////        var x = helper.executeSql(uw.ConnectionString, sqll);
                        ////////
                        ////////
                        ////////        if (x == 1)
                        ////////            setLogSuccess(dateList[i] + " eklendi");
                        ////////        else
                        ////////            setLogError(dateList[i] + " eklenemed.");
                        ////////    }
                        ////////
                        ////////}

                        string sqlMiktarlar = string.Format(@"SELECT *
                                                   FROM hisseAmounts 
                                                   WHERE  dateHisse>='{0}' and dateHisse <='{1}' ORDER BY dateHisse ASC", DateTime.Now.ToString("2018-11-01 00:00:00"), DateTime.Now.ToString("2019-12-31 00:00:00"));

                        ////////string sqlMiktarlar = string.Format(@"SELECT *
                        ////////                   FROM hisseAmounts 
                        ////////                   WHERE  dateHisse>='{0}' and dateHisse <='{1}' ORDER BY dateHisse ASC", zamanStart.ToString("yyyy-MM-dd HH:mm:ss"), zamanEnd.ToString("yyyy-MM-dd HH:mm:ss"));

                        var sonucMiktarlar = helper.getRowInSql(uw, sqlMiktarlar);

                        List<DateTime> dateListKarar = new List<DateTime>();
                        List<double> btcCloseAmountKarar = new List<double>();
                        List<double> btcEma7Amount = new List<double>();
                        List<double> btcEma24Amount = new List<double>();
                        List<double> btcRsıAmount = new List<double>();
                        List<double> rsiGreaterSeksen = new List<double>();
                        List<double> ema7GreaterSeksen = new List<double>();

                        int countList = 0;
                        if (sonucMiktarlar.Count != 0)
                        {
                            foreach (var itemM in sonucMiktarlar)
                            {

                                dateListKarar.Add(Convert.ToDateTime(itemM[7]));
                                btcCloseAmountKarar.Add(Convert.ToDouble(itemM[2]));

                                // update işlemi yapılacak i ema7 ve ema24 eklenecek.
                                if (Convert.ToDouble(itemM[3]) == 0 || Convert.ToDouble(itemM[4]) == 0)
                                {
                                    double newEma7 = ((btcCloseAmountKarar[countList] - btcEma7Amount[countList - 1]) * (2.0 / (7 + 1))) + btcEma7Amount[countList - 1];
                                    double newEma24 = ((btcCloseAmountKarar[countList] - btcEma24Amount[countList - 1]) * (2.0 / (24 + 1))) + btcEma24Amount[countList - 1];

                                    btcEma7Amount.Add(newEma7);
                                    btcEma24Amount.Add(newEma24);

                                    string sqlUpdateBtc = string.Format(@"UPDATE hisseAmounts SET ema7Amount={0} , ema24Amount={1} WHERE dateHisse='{2}'", newEma7.ToString().Replace(",", "."), newEma24.ToString().Replace(",", "."), dateListKarar[countList].ToString("yyyy-MM-dd HH:mm:ss"));


                                    var sonucUpdateBtc = helper.executeSql(uw.ConnectionString, sqlUpdateBtc);

                                    if (sonucUpdateBtc == 1)
                                        setLogSuccess("Ema7: " + newEma7 + " Ema24:" + newEma24);
                                    else
                                        setLogError("Emalar güncellenemedi.");

                                }
                                else
                                {
                                    btcEma7Amount.Add(Convert.ToDouble(itemM[3]));
                                    btcEma24Amount.Add(Convert.ToDouble(itemM[4]));
                                }

                                if (lastOperation != "AL" || string.IsNullOrEmpty(lastOperation))
                                {
                                    if (btcEma7Amount.Count > 5)
                                    {
                                        if (btcEma7Amount[btcEma7Amount.Count - 5] > btcEma7Amount[btcEma7Amount.Count - 4] && btcEma7Amount[btcEma7Amount.Count - 4] > btcEma7Amount[btcEma7Amount.Count - 3] &&
                                            btcEma7Amount[btcEma7Amount.Count - 3] < btcEma7Amount[btcEma7Amount.Count - 2] && btcEma7Amount[btcEma7Amount.Count - 2] < btcEma7Amount[btcEma7Amount.Count - 1])
                                        {
                                            double emaFark = btcEma7Amount[countList] - btcEma24Amount[countList];

                                            if (emaFark < 0)
                                                emaFark = emaFark * -1;
                                            if (emaFark > 0.01 && btcEma7Amount[countList] < btcEma24Amount[countList])
                                                decisionStepAlis1 = true;
                                        }
                                        else
                                            decisionStepAlis1 = false;
                                    }
                                    else
                                        decisionStepAlis1 = false;
                                }

                                if (Convert.ToDouble(itemM[5]) == 0)
                                {
                                    List<double> rsiList = new List<double>();

                                    for (int i = 14; i > -1; i--)
                                    {
                                        rsiList.Add(btcCloseAmountKarar[countList - i]);
                                    }
                                    var xadax = CalculateRsi(rsiList);
                                    setLogSuccess(xadax + " ");

                                    xadax = Math.Round(xadax, 2);

                                    string sqlUpdateBtc = string.Format(@"UPDATE hisseAmounts SET rsiAmount={0} WHERE dateHisse='{1}'", xadax.ToString().Replace(",", "."), dateListKarar[countList].ToString("yyyy-MM-dd HH:mm:ss"));

                                    var sonucUpdateBtc = helper.executeSql(uw.ConnectionString, sqlUpdateBtc);

                                    btcRsıAmount.Add(xadax);

                                }
                                else
                                    btcRsıAmount.Add(Convert.ToDouble(itemM[4]));

                                //if(waitOperation< dateListKarar[countList])
                                //{
                                if (lastOperation != "AL" || string.IsNullOrEmpty(lastOperation))
                                {
                                    if (decisionStepAlis1 == true)
                                    {
                                        double rsiToplam = (btcRsıAmount[countList - 1] + btcRsıAmount[countList]) / 2;

                                        int emaLimit = 0;
                                        if (isBogaSeason == true)
                                            emaLimit = 44;
                                        else
                                            emaLimit = 44;

                                        if (rsiToplam < emaLimit)
                                        {
                                            alCount++;
                                            if (alCount == 1)
                                            {
                                                alCount = 0;
                                                decisionStepAlis2 = true;
                                                operationAmount = btcCloseAmountKarar[countList];
                                                operationDate = dateListKarar[countList];
                                                lastOperation = "AL";
                                                alinanMiktar = btcCloseAmountKarar[countList];
                                                // FileWriteTxt(dateListKarar[countList], btcCloseAmountKarar[countList], lastOperation, "aliSatis2018-2");

                                                string sqlDecision = string.Format(@"SELECT decision
                                           FROM hisseAmounts 
                                           WHERE dateHisse='{0}'", dateListKarar[countList].ToString("yyyy-MM-dd HH:mm:ss"));

                                                var sonucDecision = helper.getRowInSql(uw, sqlDecision);

                                                if (sonucDecision.Count == 1)
                                                {
                                                    foreach (var itemTake in sonucDecision)
                                                    {
                                                        var decision = itemTake[0];

                                                        if (decision == null || decision.ToString()==string.Empty)
                                                        {
                                                            string sqlUpdateBtcDecisionTake = string.Format(@"UPDATE hisseAmounts SET decision='AL' WHERE dateHisse='{0}'", dateListKarar[countList].ToString("yyyy-MM-dd HH:mm:ss"));

                                                            var sonucUpdateBtcDecisionTake = helper.executeSql(uw.ConnectionString, sqlUpdateBtcDecisionTake);

                                                            var telegramIDs = helper.getConfig("telegramIDs", Application.StartupPath + "\\config.xml").Split(',');

                                                            foreach (var item in telegramIDs)
                                                            {
                                                                //sendMessage(item, " Hisse : Garanti Bank  " + Environment.NewLine + "Fiyat = " + Math.Round(alinanMiktar,2) + " " + Environment.NewLine + "Karar = AL" + Environment.NewLine + "Tarih = " + operationDate);
                                                                FileWriteTxt(dateListKarar[countList], Math.Round(btcCloseAmountKarar[countList],2), lastOperation, "aliSatisGaranti");
                                                            }
                                                        }

                                                    }

                                                }


                                            }
                                            else
                                                decisionStepAlis2 = false;

                                        }
                                        else
                                            decisionStepAlis2 = false;
                                    }
                                }

                                //}


                                if (lastOperation != "SAT" && !string.IsNullOrEmpty(lastOperation))
                                {
                                    double maxLimit = 0;
                                    int maxDay = 0;
                                    if (isBogaSeason)
                                    {
                                        maxLimit = 750;
                                        maxDay = 45;
                                    }
                                    else
                                    {
                                        maxLimit = 250;
                                        maxDay = 5;
                                    }

                                    #region garanti 1. Sat Algo

                                    if (btcRsıAmount[countList] > 40 && (btcEma7Amount[countList] - btcEma24Amount[countList] >= 0 && btcEma7Amount[countList] - btcEma24Amount[countList] <= 0.025))
                                    {
                                        isKesisti = true;
                                    }

                                    if (isKesisti == true)
                                    {

                                        if (btcEma7Amount[countList - 2] > btcEma7Amount[countList - 1] && btcEma7Amount[countList - 1] > btcEma7Amount[countList])
                                        {
                                            if (operationAmount < btcCloseAmountKarar[countList])
                                            {
                                                lastOperation = "SAT";
                                                ////FileWriteTxt(dateListKarar[countList], btcCloseAmountKarar[countList], lastOperation, "aliSatis2018-2");
                                                SatilanMiktar = btcCloseAmountKarar[countList];

                                                if (SatilanMiktar - alinanMiktar > 0)
                                                    toplamKar += SatilanMiktar - alinanMiktar;
                                                else
                                                    toplamZarar += SatilanMiktar - alinanMiktar;

                                                isKesisti = false;
                                                isZarar = false;

                                                string sqlDecision = string.Format(@"SELECT decision
                                           FROM hisseAmounts 
                                           WHERE dateHisse='{0}'", dateListKarar[countList].ToString("yyyy-MM-dd HH:mm:ss"));

                                                var sonucDecision = helper.getRowInSql(uw, sqlDecision);

                                                if (sonucDecision.Count == 1)
                                                {
                                                    foreach (var itemSell in sonucDecision)
                                                    {
                                                        var decision = itemSell[0];

                                                        if (decision == null || decision.ToString() == string.Empty)
                                                        {

                                                            string sqlUpdateBtcDecisionSell = string.Format(@"UPDATE hisseAmounts SET decision='SAT' WHERE dateHisse='{0}'", dateListKarar[countList].ToString("yyyy-MM-dd HH:mm:ss"));

                                                            var sonucUpdateBtcSell = helper.executeSql(uw.ConnectionString, sqlUpdateBtcDecisionSell);

                                                            var telegramIDs = helper.getConfig("telegramIDs", Application.StartupPath + "\\config.xml").Split(',');

                                                            foreach (var item in telegramIDs)
                                                            {
                                                                //sendMessage(item, " Hisse : Garanti Bank  " + Environment.NewLine + "Fiyat = " + Math.Round(SatilanMiktar,2) + " " + Environment.NewLine + " Karar = SAT" + Environment.NewLine + "Tarih = " + dateListKarar[countList]);
                                                                FileWriteTxt(dateListKarar[countList], Math.Round(btcCloseAmountKarar[countList],2), lastOperation, "aliSatisGaranti");
                                                            }
                                                        }

                                                    }

                                                }



                                            }
                                            else
                                                isZarar = true;
                                        }

                                    }

                                    if (isZarar == true && btcRsıAmount[countList] > 81)
                                    {

                                        lastOperation = "SAT";
                                        //FileWriteTxt(dateListKarar[countList], btcCloseAmountKarar[countList], lastOperation, "aliSatis2018-2");
                                        SatilanMiktar = btcCloseAmountKarar[countList];

                                        if (SatilanMiktar - alinanMiktar > 0)
                                            toplamKar += SatilanMiktar - alinanMiktar;
                                        else
                                            toplamZarar += SatilanMiktar - alinanMiktar;

                                        isKesisti = false;
                                        isZarar = false;

                                        string sqlUpdateBtc = string.Format(@"UPDATE hisseAmounts SET decision='SAT' WHERE dateHisse='{0}'", dateListKarar[countList].ToString("yyyy-MM-dd HH:mm:ss"));

                                        var sonucUpdateBtc = helper.executeSql(uw.ConnectionString, sqlUpdateBtc);

                                        var telegramIDs = helper.getConfig("telegramIDs", Application.StartupPath + "\\config.xml").Split(',');

                                        foreach (var item in telegramIDs)
                                        {
                                            //sendMessage(item, " Hisse : Garanti Bank  " + Environment.NewLine + "Fiyat = " + Math.Round(SatilanMiktar,2) + " " + Environment.NewLine + " Karar = SAT" + Environment.NewLine + "Tarih = " + dateListKarar[countList]);
                                            FileWriteTxt(dateListKarar[countList], Math.Round(btcCloseAmountKarar[countList],2), lastOperation, "aliSatisGaranti");
                                        }
                                    }
                                    #endregion

                                }

                                countList++;

                            }

                        }
                    }
                    catch (Exception a)
                    {
                        sendMessagehATA("575283572", " HATA BTC TRADİNG BOT...");
                        helper.setDebug(this.txtLog.Text, "hata_garanti");
                    }
                    finally
                    {
                        timer1.Enabled = true;
                        uw.Disconnect();
                        uw.Dispose();
                    }
                }
                if (hisseName == "Aselsan" && hisseShortName == "asels")
                {

                }
                if (hisseName == "Turk Hava Yollari AO" && hisseShortName == "thyao")
                {

                }
                if (hisseName == "Petkim" && hisseShortName == "petkm")
                {

                }
                if (hisseName == "Halk Bankasi" && hisseShortName == "halkb")
                {

                }
                if (hisseName == "Turk Telekomunikasyon" && hisseShortName == "ttkom")
                {

                }
                if (hisseName == "Akbank " && hisseShortName == "akbnk")
                {

                }
                if (hisseName == "Turkiye Petrol Rafinerleri" && hisseShortName == "tuprs")
                {

                }
                if (hisseName == "Pegasus" && hisseShortName == "pgsus")
                {

                }
                if (hisseName == "Yapı ve Kredi Bankasi" && hisseShortName == "ykbnk")
                {

                }
                if (hisseName == "Koza Anadolu Metal Madencilik" && hisseShortName == "kozaa")
                {

                }
                if (hisseName == "Turkiye Vakıflar Bankasi" && hisseShortName == "vakbn")
                {

                }
                if (hisseName == "Alcatel Lucent Teletas Telekomunikasyon" && hisseShortName == "alctl")
                {

                }
                if (hisseName == "Otokar Otomativ ve Savunma Sanayi" && hisseShortName == "otkar")
                {

                }


            }

        }
        public static double CalculateRsi(IEnumerable<double> closePrices)
        {
            var prices = closePrices as double[] ?? closePrices.ToArray();

            double sumGain = 0;
            double sumLoss = 0;
            int countGain = 0;
            int countLoss = 0;
            for (int i = 1; i < prices.Length; i++)
            {
                var acv = prices[i];
                var fsafa = prices[i - 1];
                var difference = prices[i] - prices[i - 1];
                if (difference >= 0)
                {
                    countGain++;
                    sumGain += difference;
                }
                else
                {
                    countLoss++;
                    sumLoss -= difference;
                }
            }

            if (sumGain == 0) return 0;
            // if (Math.Abs(sumLoss) < Tolerance) return 100;

            //var relativeStrength = sumGain / sumLoss;

            double avarageGain = sumGain / 14;
            double avarageLoss = sumLoss / 14;
            double FirstRs = avarageGain / avarageLoss;

            var relativeStrength = 100.0 - (100.0 / (1 + FirstRs));



            double step1 = 100.0 - (100.0 / (1 + relativeStrength));
            return 100.0 - (100.0 / (1 + FirstRs));



        }

        public void FileWriteTxt(DateTime decisionTime, double amount, string lastOperation, string fileName)
        {
            string DosyaYolu = Application.StartupPath + "\\" + fileName + ".txt";

            string projectName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;

            FileStream fs = new FileStream(DosyaYolu, FileMode.Append, FileAccess.Write, FileShare.Write);
            StreamWriter sw = new StreamWriter(fs);

            //sw.WriteLine("Customer Code= " + customerCode + " Son Login Zaman = " + gun + "/" + ay + "/" + yil + " " + saat + "");
            //sw.WriteLine(dateListKarar[countList] + " " + btcCloseAmountKarar[countList] + " AL");
            sw.WriteLine(decisionTime + " " + amount + " " + " " + lastOperation);
            sw.Close();
            fs.Close();
        }

        public async Task sendMessage(string destID, string text)
        {
            try
            {
                var bot = new Telegram.Bot.TelegramBotClient("962696145:AAEF_i2EgDGQIRkGEg-oaXVGVmisMnFul24");
                await bot.SendTextMessageAsync(destID, text);

            }
            catch (Exception e)
            {
                Console.WriteLine("err");
            }
        }

        public async Task sendMessagehATA(string destID, string text)
        {
            try
            {
                var bot = new Telegram.Bot.TelegramBotClient("840310650:AAF4IuOk6XlxJj6ccYNoCwFlltC2jxlOUfg");
                await bot.SendTextMessageAsync(destID, text);

            }
            catch (Exception e)
            {
                Console.WriteLine("err");
            }
        }
    }
}
