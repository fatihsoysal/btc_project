﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gerceklesenIslemParibuBot
{
    public class MarketMatch
    {
        public string dt { get; set; }
        public string mp { get; set; }
        public string ma { get; set; }
        public string tr { get; set; }

        //deneme
        public int xd { get; set; }
    }

    public class UserMatch
    {
        public string dt { get; set; }
        public string mp { get; set; }
        public string ma { get; set; }
        public string tr { get; set; }
        public string rl { get; set; }
        public string cr { get; set; }
        public int xd { get; set; }
    }

    public class RootObject
    {
        public string marketName { get; set; }
        public List<object> chartData { get; set; }
        //public SellOrders sellOrders { get; set; }
        //public BuyOrders buyOrders { get; set; }
        public List<MarketMatch> marketMatches { get; set; }
        public List<UserMatch> userMatches { get; set; }
    }
}
