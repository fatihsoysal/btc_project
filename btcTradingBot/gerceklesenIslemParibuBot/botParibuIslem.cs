﻿
using Newtonsoft.Json;
using pendata_btcTrader;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace gerceklesenIslemParibuBot
{
    public partial class botParibuIslem : Form
    {
        #region Değişkenler

        public int sn { get; set; }
        public string siteConfig { get; set; }

        string json;

        public enum logTip
        {
            error, warning, success, info
        }

        public int rowCount { get; set; }

        HttpWebRequest requestDagur { get; set; }

        CookieContainer cookieDagur { get; set; }
        //private static readonly TelegramBotClient Bot = new TelegramBotClient("563571585:AAHCTruK6lOkwHrs4slnOjA7U4TR3JB2l-0");

        #endregion

        #region Fonks

        public void setLog(string txt, Color renk)
        {
            try
            {
                if (renk == null)
                    renk = Color.Gray;

                this.rowCount++;
                if (this.rowCount > 500)
                {
                    helper.setDebug(this.txtLog.Text, "islemLoglari_paribuBot_");
                    this.rowCount = 0;
                    this.txtLog.Text = "";
                    this.txtLog.Refresh();
                    this.Refresh();
                    Application.DoEvents();
                }

                this.txtLog.SelectionStart = this.txtLog.TextLength;
                this.txtLog.SelectionLength = 0;

                this.txtLog.SelectionColor = renk;
                this.txtLog.AppendText(string.Format("[{0:HH:mm:ss}] {1}\r\n", DateTime.Now, txt));
                this.txtLog.SelectionColor = this.txtLog.ForeColor;

                this.txtLog.SelectionStart = this.txtLog.Text.Length;
                this.txtLog.ScrollToCaret();

                this.txtLog.Refresh();
                this.Refresh();
                Application.DoEvents();
            }
            catch
            {

            }
        }

        public void setLog(string txt, logTip tip = logTip.info)
        {
            Color renk;
            if (tip == logTip.error)
                renk = Color.Red;
            else if (tip == logTip.success)
                renk = Color.DarkGreen;
            else if (tip == logTip.warning)
                renk = Color.Orange;
            else
                renk = Color.LightGray;


            this.setLog(txt, renk);
        }

        public void setLogError(string txt)
        {
            this.setLog(txt, logTip.error);
        }

        public void setLogSuccess(string txt)
        {
            this.setLog(txt, logTip.success);
        }

        public void setLogWarning(string txt)
        {
            this.setLog(txt, logTip.warning);
        }

        /// <summary>
        /// request lere genel ayar yapmak için
        /// </summary>
        /// <param name="req"></param>
        /// <param name="cook"></param>
        private void setRequest(HttpWebRequest req, CookieContainer cook)
        {
            req.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3402.0 Safari/537.36";
            req.CookieContainer = cook;
        }

        private string getData(HttpWebRequest req)
        {
            string ret = "";

            req.Method = "GET";

            var response = (HttpWebResponse)req.GetResponse();
            ret = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return ret;
        }

        private string postData(HttpWebRequest req, string data)
        {
            string ret = "";

            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = data.Length;

            var dt = Encoding.ASCII.GetBytes(data);

            using (var stream = req.GetRequestStream())
            {
                stream.Write(dt, 0, dt.Length);
            }

            var response = (HttpWebResponse)req.GetResponse();

            ret = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return ret;
        }

        #endregion

        #region init & event
        public botParibuIslem()
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            this.sn = 0;
            rowCount = 0;
            cookieDagur = new CookieContainer();
            InitializeComponent();
        }

        private void botParibuIslem_Load(object sender, EventArgs e)
        {
            this.timer1.Enabled = true;
        }

        private void botParibuIslem_Shown(object sender, EventArgs e)
        {
            this.Text = "paribu Bot - ";

            this.Refresh();
            this.timer1.Tick += Timer1_Tick;
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            this.Refresh();
            this.sn++;

            if (this.sn % 5 == 0)
            {

                this.Refresh();
                this.timer1.Enabled = false;
                this.button1.Enabled = false;
                this.start();
            }
        } 
        #endregion

        private XmlNodeList getNodeList(string xmlText, string xPath)
        {

            var doc = new XmlDocument();
            doc.LoadXml(xmlText);
            return doc.SelectNodes(xPath);
        }

        private string getAttributeValue(XmlNode node, string attributeName)
        {
            try
            {
                var att = node.Attributes.GetNamedItem(attributeName);
                if (att == null)
                    return "";
                else
                    return (att.Value);
            }
            catch (Exception a)
            {
                MessageBox.Show(a.ToString());
                return "";
            }

        }

        private void start()
        {
            timer1.Enabled = false;

            XmlDocument doc = new XmlDocument();

            doc.Load(AppDomain.CurrentDomain.BaseDirectory + "config.xml");

            var uw = helper.getUW(Program.connectionMain);

            try
            {
                string html = "";

                Uri url = new Uri("https://www.paribu.com/market?market=btc_tl");
          
                using (var client = new WebClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    html = client.DownloadString(url);
 
                }

                var json = JsonConvert.DeserializeObject<gerceklesenIslemParibuBot.RootObject>(html);

                foreach (var item in json.marketMatches)
                {
                    DateTime islemZaman = helper.GetDateTime(item.dt);
                    double tlMiktar = helper.getDouble(item.mp);
                    double btcMiktar = helper.getDouble(item.ma);
                    string type = item.tr;
                    double gerceklesenMiktar = tlMiktar * btcMiktar;
                    int islemID = item.xd;


                    //gerceklesenIslemler xx  = new gerceklesenIslemler(uw);
                    //xx.addDate = DateTime.Now;
                    //xx.Save();
                    //uw.CommitChanges();

                    string sql = string.Format(@"SELECT *
                                           FROM gerceklesenIslemler 
                                           WHERE  islemZaman='{0}' and islemID={1}  ",islemZaman.ToString("yyyy-MM-dd HH:mm:ss"),islemID);

                    var sonuc = helper.getRowInSql(uw, sql);

                    if(sonuc.Count == 0)
                    {

                        var sqll = string.Format(@"

                                      	     INSERT   borsaTradingBot.dbo.gerceklesenIslemler
                                      	             ( tip,
                                      	               islemZaman,
                                      	               islemID,
                                      	               tlTutar,
                                      	               btcTutar,
                                                       gerceklesenMiktar,
                                                       addDate
                                      	              
                                      	             )
                                      	     VALUES
                                      	             ( 
                                      	               '{0}',
                                                       '{1}',
                                                        {2},
                                                        {3},
                                      	                {4},
                                                        {5},
                                                       '{6}'  
                                      	             )
                                          ", type, islemZaman.ToString("yyyy-MM-dd HH:mm:ss"),islemID, tlMiktar.ToString().Replace(",", "."), btcMiktar.ToString().Replace(",", "."), gerceklesenMiktar.ToString().Replace(",", "."),DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                        var x = helper.executeSql(uw.ConnectionString, sqll);


                        if (x == 1)
                            setLogSuccess(islemID+" eklendi");
                        else
                            setLogError(+islemID+" eklenemed.");

                    }
                }




                
                
            }
            catch (Exception a)
            {

            }
            finally
            {
                timer1.Enabled = true;
                uw.Disconnect();
                uw.Dispose();
            }



        }
    }
}
