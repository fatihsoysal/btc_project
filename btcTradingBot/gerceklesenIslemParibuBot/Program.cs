﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gerceklesenIslemParibuBot
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new botParibuIslem());
        }

        public static string configPath
        {
            get
            {
                return Application.StartupPath + "\\config.xml";
            }
        }

        public static string connectionMain
        {
            get
            {
                return helper.getConfig("connection", Program.configPath);
            }
        }
    }
}
